This is a project to test how function call happenes between the iOS and the web page.
To load a web page, iOS uses [WKWebView](https://developer.apple.com/documentation/webkit/wkwebview).

## Call a function from the iOS to the web page
- The iOS can use WebKit's `evaluateJavaScript` function
	- ```func evaluateJavaScript(_ javaScriptString: String, completionHandler: ((Any?, Error?) -> Void)? = nil)```
- By using the `completionHandler`, iOS can get the **return value** from the web page
- If a web page is build by **React**
	- React component can't respond to the function call invoked by iOS
	- The target javascript function should be defined **outside** of the React world and has to be connected with the React component
- Examples
	```
	(swift)
	evaluateJavaScript(javascriptCode) { (result, error) in
		guard error == nil else {
			print("error : \(error!)")
			return
		}

		if
			let result = result,
			let completionHandler = completionHandler {
			completionHandler(result)
		}
	}
	```

## Call a function from the web page to the iOS
- The web page can use window.webkit.messageHandlers.{MethodName}.postMessage(argument: Any)
	- https://developer.apple.com/documentation/webkitjs/usermessagehandler/1631164-postmessage
- To make `webkit.messageHandlers.{MethodName}.postMessage(arguments: Any)` work, iOS has to register the {MethodName} for the WKWebview **in advance** to load the web page
	```
	(swift)
	userContentController.addScriptMessageHandler(
		self,
		contentWorld: .page,
		name: {MethodName}
	)
	```
- Example
	```
	(javascript)
	var promise = window.webkit.messageHandlers.fetchFromIOS.postMessage("hello from web view");
	promise.then(
		function (result) {
			setText(result);
		},
		function (err) {
			setText(err);
		}
	);
	```

