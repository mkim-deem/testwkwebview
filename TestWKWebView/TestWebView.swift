//
//  TestWebView.swift
//  TestWKWebView
//
//  Created by MinJeong Kim on 02/02/2022.
//

import Foundation
import WebKit

class TestWebView: WKWebView {
    var parentVC: UIViewController!
    
    private override init(frame: CGRect, configuration: WKWebViewConfiguration) {
        super.init(frame: frame, configuration: configuration)
        self.uiDelegate = self
    }
    
    convenience init(parentVC: UIViewController, frame: CGRect) {
        let customConfig = WKWebViewConfiguration()
        
        // important - assigning should happen before `self.init`
        let userContentController = WKUserContentController()
        customConfig.userContentController = userContentController
        
        // save token in the local storage
        let setTokenScript = WKUserScript(
            source: "window.localStorage.setItem('token', 'token set by iOS');",
            injectionTime: .atDocumentStart,
            forMainFrameOnly: true
        )
        customConfig.userContentController.addUserScript(setTokenScript)
        
        self.init(frame: frame, configuration: customConfig)

        // Installs a message handler that you can call from your JavaScript code.
        // It can not return a value to the web
        // userContentController.add(self, name: "fetchFromIOS")
        
        // Installs a message handler that returns a reply to your JavaScript code.
        userContentController.addScriptMessageHandler(
            self,
            contentWorld: .page,
            name: "fetchFromIOS"
        )
        
        self.parentVC = parentVC
        self.uiDelegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func evaluateJavaScriptFunction(functionName: String?, params: String?..., completionHandler: ((Any)->Void)? = nil) {
        if let functionName = functionName {
            var javascriptCode: String
            
            if !params.isEmpty {
                let paramStringArray = params.map{ "\"\($0!)\"" }
                let paramString = paramStringArray.joined(separator: ",")
                javascriptCode = "\(functionName)(\(paramString));"
            } else {
                javascriptCode = "\(functionName)();"
            }
            
            print("[iOS -> Javascript Call] \(javascriptCode)")
            evaluateJavaScript(javascriptCode) { (result, error) in
                guard error == nil else {
                    print("error : \(error!)")
                    return
                }
                
                if
                    let result = result,
                    let completionHandler = completionHandler {
                    completionHandler(result)
                }
            }
        }
    }
}

extension TestWebView: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "fetchFromIOS", let messageBody = message.body as? String {
            print(messageBody)
        }
    }
}

extension TestWebView: WKScriptMessageHandlerWithReply {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage, replyHandler: @escaping (Any?, String?) -> Void) {
        if message.name == "fetchFromIOS", let messageBody = message.body as? String {
            print(messageBody)
            replyHandler("return value from iOS", nil)
        }
    }
}

extension TestWebView: WKUIDelegate {
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo) async {
        let alert = UIAlertController(
            title: nil,
            message: message,
            preferredStyle: .alert
        )
        
        let okAction = UIAlertAction(title: "ok", style: .default)
        alert.addAction(okAction)
        
        parentVC.present(alert, animated: true, completion: nil)
    }
}
