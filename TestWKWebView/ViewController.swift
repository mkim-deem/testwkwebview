//
//  ViewController.swift
//  TestWKWebView
//
//  Created by MinJeong Kim on 02/02/2022.
//

import UIKit


class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func vanillaJSButtonTouched(_ sender: Any) {
        let vc = VanillaWebViewController(nibName: "VanillaWebViewController", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func reactButtonTapped(_ sender: Any) {
        let vc = ReactWebViewController(nibName: "ReactWebViewController", bundle: nil)
        navigationController?.pushViewController(vc, animated: true)
    }
}


