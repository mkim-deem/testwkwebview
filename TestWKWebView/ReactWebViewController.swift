//
//  ReactWebViewController.swift
//  TestWKWebView
//
//  Created by MinJeong Kim on 02/02/2022.
//

import UIKit
import WebKit

class ReactWebViewController: UIViewController {
    var webview: TestWebView!
    @IBOutlet weak var callJSButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.webview = TestWebView(parentVC: self, frame: self.view.bounds)
        
        // add webview
        webview.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(webview)
        webview.topAnchor.constraint(equalTo: callJSButton.bottomAnchor).isActive = true
        webview.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        webview.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        webview.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        // load local html
        guard let localHtmlUrl = Bundle.main.url(forResource: "react", withExtension: "html") else {
            return
        }
        webview.load(URLRequest(url: localHtmlUrl))

    }
    
    @IBAction func callJSButtonTouched(_ sender: Any) {
        webview.evaluateJavaScriptFunction(functionName: "iOSToWeb", params: "1-token", "2-whatever") { result in
            print(result)
        }
    }
}

extension ReactWebViewController: WKScriptMessageHandlerWithReply {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage, replyHandler: @escaping (Any?, String?) -> Void) {
        if message.name == "fetchFromIOS", let messageBody = message.body as? String {
            print(messageBody)
            replyHandler("return value from iOS", nil)
        }
    }
    
}

